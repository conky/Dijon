﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dijon.Models
{
    public class BotConfig
    {

        public string botToken { get; set; }
        public string clientID { get; set; }

    }
}
